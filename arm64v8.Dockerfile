# Build Aliyun CLI
FROM arm64v8/golang:1.14.4-alpine3.11 as builder

# Default version to build is version 3.0.46
ARG ALIYUN_CLI_VERSION=3.0.46

RUN apk add --no-cache git jq build-base && \
    git clone https://github.com/aliyun/aliyun-openapi-meta && \
    git clone https://github.com/aliyun/aliyun-cli.git && \
    cd aliyun-cli/ && git checkout tags/v${ALIYUN_CLI_VERSION} && \
    make deps && make build && cp out/aliyun /usr/local/bin/aliyun

FROM arm64v8/alpine:3.11
COPY --from=builder /usr/local/bin/aliyun /usr/local/bin/aliyun
ENTRYPOINT [ "/usr/local/bin/aliyun" ]
