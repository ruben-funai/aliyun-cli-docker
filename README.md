# Aliyun CLI (Alpine Linux)

[![pipeline status](https://gitlab.com/ruben-funai/aliyun-cli-docker/badges/master/pipeline.svg) ![Docker Image Version (latest by date)](https://img.shields.io/docker/v/rubenfunai/aliyun-cli) ![Docker Pulls](https://img.shields.io/docker/pulls/rubenfunai/aliyun-cli?label=pulls) ![Docker Image Size (tag)](https://img.shields.io/docker/image-size/rubenfunai/aliyun-cli/latest?label=size)](https://gitlab.com/ruben-funai/aliyun-cli-docker/-/pipelines/latest)

This docker image contains the Alpine Linux compiled Aliyun CLI binary (Alibaba Cloud Command Line Interface).

# Usage (CLI)

You may use the CLI like so: 

```bash
docker run rubenfunai/aliyun-cli oss ls \
  --mode AK \
  --region cn-beijing \
  --access-key-id xxxx \
  --access-key-secret xxxx
```

# Usage (Dockerfile)

If you need to use the binary from within your Docker image, you may copy it in like so:

```Dockerfile
FROM rubenfunai/aliyun-cli:latest as aliyun-cli-image

FROM alpine:latest

COPY --from=aliyun-cli-image /usr/local/bin/aliyun /usr/local/bin/aliyun
```

# Build details

Every 4 hours, new versions are checked for and built into images.  
The newest version is applied the `latest` tag.  

Aliyun CLI Source: [https://github.com/aliyun/aliyun-cli](https://github.com/aliyun/aliyun-cli)  
CI/CD Source: [https://gitlab.com/ruben-funai/aliyun-cli-docker](https://gitlab.com/ruben-funai/aliyun-cli-docker)  
Docker Hub: [https://hub.docker.com/r/rubenfunai/aliyun-cli](https://hub.docker.com/r/rubenfunai/aliyun-cli)

# Motivation

The Aliyun CLI binaries provided by Aliyun have been compiled using the [GNU C Library (glibc)](https://www.gnu.org/software/libc/). Alpine Linux
 uses [musl libc](https://musl.libc.org/) rather than glibc and so the binaries provided by Aliyun will not run on Alpine Linux. 
 Use these images to copy in the Aliyun binary for use in your Alpine Linux based images.

The Aliyun CLI binaries provided by Aliyun are only compiled for `amd64`. This repo aims to provide images containing the binary built for other architectures as well (arm32v7, arm64v8, ppc64le and i386).

# Issues and/or Suggestions

Create a new issue [here](https://gitlab.com/ruben-funai/aliyun-cli-docker/-/issues).
